// creating objects
const circle = {
  radius: 80,
  location: {
    x: 1,
    y: 1,
  },
  draw: () => {
    console.log("draw");
  },
};
circle.draw();

// Creating objects using factories
const createCircle = (radius) => {
  return {
    radius,
    draw: () => {
      console.log("Draw");
    },
  };
};

const circle2 = createCircle(10);
circle2.draw();

// Creating objects using constructor

function Circle(radius) {
  this.radius = radius;
  (this.defaultLocation = { x: 0, y: 0 }),
    (this.computeOptimumLocation = function () {}),
    (this.draw = () => {
      this.computeOptimumLocation();
    });
}
const circle3 = new Circle(100);
circle3.draw();

// Constructor property

console.log(Circle.constructor);

Circle.call({}, 1); // => const another new Circle(1);

Circle.apply({}, [1, 2, 3, 4, 5]);

//  In JS functions are objects

// Primitive Variables in JS are independent variables
// That is
let num1 = 10;
let num2 = num1;

num1 = 20;
console.log(num2);

//it is not the case of objects

let obj1 = { value: 200 };
let obj2 = obj1;
obj1.value = 100;
console.log(obj2);

// Primitives are copied by their value
// Objects are copied by their reference

const circle5 = new Circle(10);
for (let key in circle5) {
  console.log(key, circle[key]);
}
for (let key in circle5) {
  if (typeof circle[key] !== "function") console.log(key, circle[key]);
}
console.log(Object.keys());

if ("radius" in circle) {
  console.log();
}

// Excercise

function StopWatch() {
  let startTime,
    endTime,
    running,
    duration = 0;

  this.start = function () {
    if (running) {
      throw new Error("StopWatch has already started");
    }
    running = true;
    startTime = new Date();
  };

  this.stop = function(){
    if(!running){
        throw new Error('Stopwatch is not started');
    }
    running = false;
    endTime = new Date();

    duration += (endTime.getTime() - startTime.getTime())/1000;
  };

  this.reset = function(){
    startTime = null;
    endTime = null;
    running = false;
    duration = 0;

  };
  Object.defineProperties(this,"duration",{
    get: function(){ return duration}
  });
}

